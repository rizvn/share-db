package app

import java.lang.Double
import java.util.Date
/**
  * Created by Riz
  */
class HistoricalPrice{
  var date: Date = _
  var open: Double = _
  var high: Double = _
  var low: Double = _
  var close: Double = _
  var volume: Double = _
  var adjClose: Double = _
}
