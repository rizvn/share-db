package app

import com.google.inject.{AbstractModule, Provides, Singleton}
import com.zaxxer.hikari.HikariDataSource
import org.skife.jdbi.v2.DBI

/**
  * Created by Riz
  */
class Config extends AbstractModule{
  override def configure(): Unit = {

  }

  @Provides
  @Singleton
  def dbConnection(): DBI ={
    val ds = new HikariDataSource()
    ds.setUsername("root")
    ds.setPassword("riz123")
    ds.setJdbcUrl("jdbc:mysql://localhost:3306/stocks")
    new DBI(ds)
  }
}


