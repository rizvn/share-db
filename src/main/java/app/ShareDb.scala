package app

import java.io.{FileInputStream, FileNotFoundException, InputStream, InputStreamReader}
import java.net.URI
import java.time.LocalDate
import java.util
import java.util.Date
import java.util.concurrent.Callable

import app.ImplicitConversions._
import com.google.common.cache.CacheBuilder
import com.google.inject.{Inject, Singleton}
import org.apache.commons.csv.CSVFormat
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.math.NumberUtils
import org.apache.commons.lang3.time.DateUtils
import org.skife.jdbi.v2.{DBI, PreparedBatchPart}
import org.skife.jdbi.v2.util.IntegerColumnMapper

import scala.collection.JavaConversions._

/**
  * Created by Riz
  */
@Singleton
class ShareDb {

  @Inject
  val dbi: DBI = null

  val labelCache = CacheBuilder.newBuilder().build[String, Integer]()
  val symbolCache = CacheBuilder.newBuilder().build[String, Integer]()


  def getBalanceSheet(symbol: String) : InputStream = {
    val url = new URI(s"http://financials.morningstar.com/ajax/exportKR2CSV.html?t=xlon:${symbol}").toURL
    url.openStream()
  }

  def loadSymbols(path: String) : util.List[String] = {
    val rows = CSVFormat.DEFAULT.parse(new InputStreamReader(new FileInputStream(path)))
    rows.map{row => row.get(0)}.toList
  }


  def fetchHistoricalPrices(symbol: String, from: LocalDate, to: LocalDate): InputStream = {
    //can only feth upto 2 years historical data
    val a = from.getMonthValue - 1
    val b = from.getDayOfMonth
    val c = from.getYear
    val d = to.getMonthValue - 1
    val e = to.getDayOfMonth
    val f = to.getYear


    val url =new URI(s"http://real-chart.finance.yahoo.com/table.csv?s=${symbol}.L&a=${a}&b=${b}&c=${c}&d=${d}&e=${e}&f=${f}&g=d&ignore=.csv").toURL
    try {
      url.openStream()
    }
    catch {
      case ex: FileNotFoundException =>
        println(s"Could not parse historical prices for ${symbol}")
        return null
    }

  }

  def parseBalanceSheet(is: InputStream, symbol: String) : util.List[KeyRatios] = {
    val reader = new InputStreamReader(is)
    val balanceCsv = CSVFormat.EXCEL.parse(reader)
    var cols = 0
    val keyRatios = new util.ArrayList[KeyRatios]()

    if(balanceCsv.getRecords.size() < 2){
      println(s"Key ratios do not exist for symbol: ${symbol}")
      return keyRatios
    }

    balanceCsv.zipWithIndex.foreach {
      case (row, index) =>
        //second row contains year headings
        if (index == 2) {
          println("Years " + row)
          cols = row.size()-1
          for(i <- 1 to cols){
            keyRatios.add(new KeyRatios(row.get(i), symbol))
          }
        }

        //data rows
        if (index > 2) {
          val label = row.get(0)
          if(StringUtils.isNotBlank(label) && row.size() > 3){
            for (i <- 0 to cols-1){
              try {
                var value = row.get(i + 1)
                value = if (StringUtils.isBlank(value)) null else value
                keyRatios.get(i).data.put(label, value)
              }catch{
                //ignore index out of bounds of exceptions for where there isnt data
                case e: ArrayIndexOutOfBoundsException =>
                  println(s"Index out of bound ${i} whilst getting row value")
              }
            }
          }
        }
    }
    keyRatios
  }

  /**
    * Fetch pk from cache/db or add to db
    * @param label
    * @return
    */
  def getPkForSymbol(label: String) : Integer = {
    labelCache.get(label, new Callable[Integer]{
      override def call(): Integer = {
        dbi.run{h =>
          //try getting the pk
          val result = h.createQuery("SELECT pk from symbols where name = :name")
            .bind("name", label)
            .map(IntegerColumnMapper.PRIMITIVE)
            .first()


          //insert value in db if not found, the return value will be cached
          if(result == null){
            return h.createStatement("INSERT INTO symbols(name) values(:name)")
              .bind("name", label)
              .executeAndReturnGeneratedKeys(IntegerColumnMapper.PRIMITIVE)
              .first()
          }
          else{
            return result
          }
        }
      }
    })
  }


  /**
    * Fetch pk from cache/db or add to db
    * @param label
    * @return
    */
  def getPkForLabel(label: String) : Integer = {
    labelCache.get(label, new Callable[Integer]{
      override def call(): Integer = {
        dbi.run{h =>
          //try getting the pk
          val result = h.createQuery("SELECT pk from labels where name = :name")
                         .bind("name", label)
                         .map(IntegerColumnMapper.PRIMITIVE)
                         .first()


          //insert value in db if not found, the return value will be cached
          if(result == null){
            return h.createStatement("INSERT INTO LABELS(name) values(:name)")
                    .bind("name", label)
                    .executeAndReturnGeneratedKeys(IntegerColumnMapper.PRIMITIVE)
                    .first()
          }
          else{
            return result
          }
        }
      }
    })
  }

  def writeToDb(historicalPrices: util.List[HistoricalPrice], symbol: String) = {
    if(!historicalPrices.isEmpty){
      dbi.run{h=>
        val symPk = getPkForSymbol(symbol)
        val batch = h.prepareBatch(
          """INSERT INTO HIST_PRICE(day, symbol, open, high, low, close, volume, adjclose)
          VALUES(:day, :symbol, :open, :high, :low, :close, :volume, :adjclose)
          """)

        historicalPrices.foreach{price =>
          val ppBatch = batch.add()
          ppBatch.bind("day", price.date)
          ppBatch.bind("symbol", symPk)
          ppBatch.bind("open", price.open)
          ppBatch.bind("high", price.high)
          ppBatch.bind("low", price.low)
          ppBatch.bind("close", price.close)
          ppBatch.bind("volume", price.volume)
          ppBatch.bind("adjclose", price.adjClose)
        }
        batch.execute()

      }
    }
  }


  def writeToDb(ratios: util.List[KeyRatios]) = {
    ratios.foreach { ratio =>
      val ratioDate = if (ratio.year.trim.startsWith("TTM"))
                         new Date()
                      else
                         DateUtils.parseDate(ratio.year, "yyyy-mm")

      ratio.data.entrySet().foreach { entry =>
        val value  = if (NumberUtils.isNumber(entry.getValue))
                       Some(NumberUtils.toDouble(entry.getValue))
                     else
                        None

        dbi.run{ handle =>
          handle.createStatement(
            """INSERT INTO KEYRATIO(ratioDate, symbol, label, value)
              VALUES (:ratioDate, :symbol, :label, :value) """
          )
          .bind("ratioDate", ratioDate)
          .bind("symbol", getPkForSymbol(ratio.symbol))
          .bind("label",  getPkForLabel(entry.getKey))
          .bind("value",  if(value.isEmpty) null else value.get)
          .execute()
        }
      }
    }
  }

  def parseHistoricalPrices(is: InputStream, symbol: String) : util.List[HistoricalPrice]= {
    val reader = new InputStreamReader(is)

    try {
      val parser = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(reader)
      val results = parser.getRecords.map{row =>
        val historicalPrice = new HistoricalPrice()
        historicalPrice.date = DateUtils.parseDate(row.get(0), "yyyy-MM-dd")
        historicalPrice.open = NumberUtils.toDouble(row.get(1))
        historicalPrice.high = NumberUtils.toDouble(row.get(2))
        historicalPrice.low = NumberUtils.toDouble(row.get(3))
        historicalPrice.close = NumberUtils.toDouble(row.get(4))
        historicalPrice.volume = NumberUtils.toDouble(row.get(5))
        historicalPrice.adjClose = NumberUtils.toDouble(row.get(6))
        historicalPrice
      }.toList

      is.close()
      results
    }
    catch {
      case ex: FileNotFoundException =>
        println(s"Could not parse historical prices for ${symbol}")
        return new util.ArrayList[HistoricalPrice]();
    }


  }
}
