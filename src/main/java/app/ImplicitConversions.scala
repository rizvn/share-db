package app

import org.skife.jdbi.v2.{DBI, Handle}

object ImplicitConversions{

  //add handle method to db instance
  implicit class DBIHandle(dbi: DBI){

    /**
      *
      * @param closure closure which takes a handle and return T
      * @tparam T return type
      * @return result of closure excution
      */
    def run[T](closure: (Handle) => T ): T = {
      val handle = dbi.open()
      try{
        return closure(handle)
      }finally {
        handle.close()
      }
    }
  }
}

