package app

import java.io.{File, FileInputStream}
import java.time.LocalDate

import com.google.inject.{Guice, Injector}
import org.apache.commons.io.FileUtils
import org.junit.{Assert, Test}

import scala.collection.JavaConversions._

/**
  * Created by Riz
  */
class ShareDbTest {

  implicit lazy val injector: Injector = {
    Guice.createInjector(new Config)
  }

  @Test
  def fetchBalanceSheet(){
    val shareDb = injector.getInstance(classOf[ShareDb])
    val is = shareDb.getBalanceSheet("BEZ")
    FileUtils.copyInputStreamToFile(is, new File("data/bez.csv"))
  }

  @Test
  def parseBalanceSheet(){
    val shareDb = injector.getInstance(classOf[ShareDb])
    val keyRatios = shareDb.parseBalanceSheet(new FileInputStream("data/BEZ.csv"), "BEZ")
    Assert.assertFalse(keyRatios.isEmpty)
  }

  @Test
  def writeToDb(){
    val shareDb = injector.getInstance(classOf[ShareDb])
    val keyRatios = shareDb.parseBalanceSheet(new FileInputStream("data/BEZ.csv"), "BEZ")
    shareDb.writeToDb(keyRatios)
  }

  @Test
  def testGetPkForLabel(){
    val shareDb = injector.getInstance(classOf[ShareDb])
    val pk = shareDb.getPkForLabel("one")
    Assert.assertNotNull(pk)
  }

  @Test
  def testGetPkForSymboll(){
    val shareDb = injector.getInstance(classOf[ShareDb])
    val pk = shareDb.getPkForSymbol("one")
    Assert.assertNotNull(pk)
  }

  @Test
  def loadSymbolsTest(): Unit ={
    val shareDb = injector.getInstance(classOf[ShareDb])
    val symbols = shareDb.loadSymbols("data/symbols.csv")
    Assert.assertTrue(symbols.size() > 200)
  }

  @Test
  def fetchHistoricalPrices(){
    val shareDb = injector.getInstance(classOf[ShareDb])
    val is = shareDb.fetchHistoricalPrices("AA", LocalDate.of(2010, 1, 1), LocalDate.now())
    FileUtils.copyInputStreamToFile(is, new File("data/aa_historical_prices.csv"))
  }


  @Test
  def writeHistoricalPricesToDb(){
    val shareDb = injector.getInstance(classOf[ShareDb])
    val historicalPrices = shareDb.parseHistoricalPrices(new FileInputStream("data/aa_historical_prices.csv"), "AA")
    shareDb.writeToDb(historicalPrices, "AA.")
  }

  @Test
  def fullRun(): Unit ={
    val shareDb = injector.getInstance(classOf[ShareDb])
    val symbols = shareDb.loadSymbols("data/symbols.csv")
    symbols.foreach{symbol =>
      try {
        val balanceSheet = shareDb.getBalanceSheet(symbol)
        val keyRatios = shareDb.parseBalanceSheet(balanceSheet, symbol)
        val historicalPricesIs = shareDb.fetchHistoricalPrices(symbol, LocalDate.of(2001, 1, 1), LocalDate.now())

        shareDb.writeToDb(keyRatios)

        if(historicalPricesIs != null) {
          val historicalPrices = shareDb.parseHistoricalPrices(historicalPricesIs, symbol)
          shareDb.writeToDb(historicalPrices, symbol)
        }

      }catch{
        case e: Exception => e.printStackTrace()
      }
    }
  }
}
