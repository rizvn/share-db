create database stocks;
use stocks;

CREATE TABLE keyratio
(
    ratioDate DATE NOT NULL,
    symbol INT NOT NULL,
    label INT NOT NULL,
    value DOUBLE
);

CREATE TABLE stocks.labels
(
    pk INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL
);
CREATE UNIQUE INDEX labels_pk_uindex ON stocks.labels (pk);
CREATE UNIQUE INDEX labels_name_uindex ON stocks.labels (name);


CREATE TABLE stocks.symbols
(
    pk INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(5) NOT NULL
);
CREATE UNIQUE INDEX symbols_pk_uindex ON stocks.symbols (pk);
CREATE UNIQUE INDEX symbols_name_uindex ON stocks.symbols(name);

CREATE TABLE stocks.hist_price
(
    day DATE NOT NULL,
    symbol INT NOT NULL,
    open DOUBLE,
    high DOUBLE,
    low DOUBLE,
    close DOUBLE,
    volume DOUBLE,
    adjclose DOUBLE
);
