-- Find all book value ratios for symbol, sorted by date
SELECT  * from keyratio ratio
LEFT JOIN symbols sym on sym.pk = ratio.symbol
LEFT JOIN labels lbl ON lbl.pk = ratio.label
WHERE lbl.name like "%book%"
AND sym.name = "PAYS"
ORDER BY ratio.ratioDate


